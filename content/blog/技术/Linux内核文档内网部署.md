+++
title = "Linux内核文档内网部署"
date = "2021-11-18T10:49:20+02:00"
tags = ["Linux",]
categories = ["技术",]
banner = "img/banners/tech.jpg"
authors = ["EricXi"]
+++


# Linux内核文档迁移内网操作手册
notes: 若想直接过去内核文档，内网部署请直接访问
链接：https://pan.baidu.com/s/1I1EJplvZIqH0HPoSzuuMUw 
提取码：6666 
## 1. 迁移概述：
外网可在http://kernel.org/doc/html查看linux内核所有版本的文档，为方便内网查看手册特迁移外网文档入内网，Linux内核文档位于项目代码Documentation目录下
文档以reStructuredText文件存放，可通过Sphinx换成为HTML或PDF或LaTex格式文档，项目中makefile文件支持使用 make htmldocs 或 make pdfdocs 命令构建HTML或PDF格式的文档，
最终生成文件位于Documentation/output文件夹中，内网通过nginx或apache搭建web服务，移动output到/var/www/目录下，改名为对应版本如v4.19,则可通过web界面直接访问文档。

## 2. 具体步骤：
### 1. 下载内核代码
```
git clone https://github.com/torvalds/linux.git
git checkout v4.19 -b v4.19 # 切换到要构建的内核版本
```
### 2. 安装Sphinx
```
# python3安装
yum -y install python3.6
python3 -m pip install virtualenv
# 其他依赖
内核文档构建系统包含一个扩展，可以处理GraphViz和SVG格式的图像为了让它工作，您需要同时安装GraphViz和ImageMagick包。

挂载方式安装texlive软件包，目前只有Sphinx 1.4及更高版本才支持这种构建。

对于PDF和LaTeX输出，还需要 XeLaTeX 3.14159265版本。（译注：此版本号真实 存在）

根据发行版的不同，您可能还需要安装一系列 texlive 软件包，这些软件包提供了 XeLaTeX 工作所需的最小功能集。
wget -c https://mirrors.tuna.tsinghua.edu.cn/CTAN/systems/texlive/Images/texlive.iso
mount -o loop texlive.iso  /mnt/

进入mnt,运行install-tl
cd /mnt
./install-tl
输入i安装
vim ~/.bashrc
PATH=/usr/local/texlive/2021/bin/x86_64-linux:$PATH; export PATH
MANPATH=/usr/local/texlive/2021/texmf-dist/doc/man:$MANPATH; export MANPATH
INFOPATH=/usr/local/texlive/2021/texmf-dist/doc/man:$INFOPATH; export INFOPATH
source ~/.bashrc

检查Sphinx依赖项 ./scripts/sphinx-pre-install --no-pdf

查看Sphinx依赖版本 cat Documentation/sphinx/requirement.txt 安装对应的sphinx
virtualvenv sphinx_1.7.9
source ./sphinx_1.7.9/bin/activate
pip install -r Documentation/sphinx/requirement.txt
```
### 3. 构建文档
```
make htmldocs
若报错请依照报错信息安装对应其他依赖，构建多版本内核请先清理之前构建信息，make cleandocs
```


